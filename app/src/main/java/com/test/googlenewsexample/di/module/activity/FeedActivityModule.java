package com.test.googlenewsexample.di.module.activity;

import android.arch.lifecycle.ViewModelProviders;

import com.test.googlenewsexample.view.feed.FeedActivity;
import com.test.googlenewsexample.viewmodel.FeedViewModel;

import dagger.Module;
import dagger.Provides;

/**
 * @author StrangeHare
 * Date: 13.05.2018
 */
@Module
public class FeedActivityModule {

    @Provides
    FeedViewModel provideViewModel(FeedActivity activity) {
        return ViewModelProviders.of(activity).get(FeedViewModel.class);
    }
}
