package com.test.googlenewsexample.di.module.activity;

import com.test.googlenewsexample.di.scope.ActivityScope;
import com.test.googlenewsexample.view.feed.FeedActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * @author StrangeHare
 * Date: 13.05.2018
 */
@Module(includes = AndroidSupportInjectionModule.class)
public interface ActivityModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = FeedActivityModule.class)
    FeedActivity feedActivityInjector();
}
