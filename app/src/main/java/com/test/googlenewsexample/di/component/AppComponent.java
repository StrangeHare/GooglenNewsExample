package com.test.googlenewsexample.di.component;

import android.content.Context;

import com.test.googlenewsexample.App;
import com.test.googlenewsexample.di.module.activity.ActivityModule;
import com.test.googlenewsexample.di.module.fragment.FragmentModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

/**
 * @author StrangeHare
 * Date: 13.05.2018
 */
@Singleton
@Component(modules = {ActivityModule.class, FragmentModule.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder setContext(Context context);

        AppComponent build();
    }

    void inject(App app);
}
