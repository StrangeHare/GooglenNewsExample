package com.test.googlenewsexample.di.module.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v4.app.FragmentActivity;

import com.test.googlenewsexample.view.feed.FeedFragment;
import com.test.googlenewsexample.viewmodel.FeedViewModel;

import dagger.Module;
import dagger.Provides;

/**
 * @author StrangeHare
 * Date: 13.05.2018
 */
@Module
public class FeedFragmentModule {

    @Provides
    FeedViewModel provideViewModel(FeedFragment fragment) {
        return ViewModelProviders.of((FragmentActivity) fragment.getActivity()).get(FeedViewModel.class);
    }
}
