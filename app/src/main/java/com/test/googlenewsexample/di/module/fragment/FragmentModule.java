package com.test.googlenewsexample.di.module.fragment;

import com.test.googlenewsexample.di.scope.FragmentScope;
import com.test.googlenewsexample.view.details.DetailsFragment;
import com.test.googlenewsexample.view.feed.FeedFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * @author StrangeHare
 * Date: 13.05.2018
 */
@Module(includes = AndroidSupportInjectionModule.class)
public interface FragmentModule {

    @FragmentScope
    @ContributesAndroidInjector(modules = FeedFragmentModule.class)
    FeedFragment feedFragmentInjector();

    @FragmentScope
    @ContributesAndroidInjector(modules = DetailsFragmentModule.class)
    DetailsFragment detailsFragmentInjector();
}
