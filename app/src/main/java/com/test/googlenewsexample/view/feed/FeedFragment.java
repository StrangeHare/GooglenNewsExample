package com.test.googlenewsexample.view.feed;

import android.arch.lifecycle.LifecycleOwner;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.googlenewsexample.R;
import com.test.googlenewsexample.databinding.FragmentFeedBinding;
import com.test.googlenewsexample.utils.SnackBarUtils;
import com.test.googlenewsexample.view.BaseFragment;
import com.test.googlenewsexample.viewmodel.FeedViewModel;

import javax.inject.Inject;

public class FeedFragment extends BaseFragment implements IFeedView {

    public static String FEED_FRAGMENT_TAG = "FEED_FRAGMENT_TAG";
    @Inject
    FeedViewModel viewModel;
    private FragmentFeedBinding binding;

    public static FeedFragment newInstance() {
        FeedFragment fragment = new FeedFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_feed, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel.setView(this);
        viewModel.getLiveData().observe((LifecycleOwner) getActivity(), news -> {
            if (news != null) {
                viewModel.addData(news.getArticles());
            }
        });
        binding.recyclerNews.setAdapter(viewModel.getAdapter());
        binding.recyclerNews.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));
        binding.recyclerNews.addItemDecoration(new DividerItemDecoration(view.getContext(),  DividerItemDecoration.VERTICAL));
    }

    @Override
    public void requestWithError() {
        SnackBarUtils.showSnackbar(binding.getRoot().getContext(), binding.getRoot(), getString(R.string.error_request_news));
    }
}
