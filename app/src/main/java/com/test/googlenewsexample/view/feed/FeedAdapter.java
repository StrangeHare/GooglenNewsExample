package com.test.googlenewsexample.view.feed;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.test.googlenewsexample.R;
import com.test.googlenewsexample.databinding.HolderNewsBinding;
import com.test.googlenewsexample.model.ArticlesItem;
import com.test.googlenewsexample.view.details.DetailsFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * @author StrangeHare
 * Date: 13.05.2018
 */
public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.NewsHolder> {

    private List<ArticlesItem> list = new ArrayList<>();

    @NonNull
    @Override
    public NewsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NewsHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.holder_news, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull NewsHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void addData(List<ArticlesItem> data) {
        list = data;
        notifyItemRangeChanged(0, list.size());
    }

    static class NewsHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

        private final HolderNewsBinding currentBinding;
        private ArticlesItem currentItem;

        NewsHolder(HolderNewsBinding binding) {
            super(binding.getRoot());
            currentBinding = binding;
            currentBinding.getRoot().setOnClickListener(this);
        }

        void bind(ArticlesItem item) {
            currentItem = item;
            currentBinding.setNews(item);
            currentBinding.executePendingBindings();

            Picasso.get().load(item.getUrlToImage()).into(currentBinding.galleryImage);
        }

        @Override
        public void onClick(View v) {
            AppCompatActivity activity = (AppCompatActivity) v.getContext();
            activity.getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, DetailsFragment.newInstance(currentItem))
                    .addToBackStack(null)
                    .commit();
        }
    }
}
