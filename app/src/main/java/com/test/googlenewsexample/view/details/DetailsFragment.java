package com.test.googlenewsexample.view.details;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.test.googlenewsexample.R;
import com.test.googlenewsexample.databinding.FragmentDetailsBinding;
import com.test.googlenewsexample.model.ArticlesItem;
import com.test.googlenewsexample.view.BaseFragment;

public class DetailsFragment extends BaseFragment {

    public final static String SELECTED_ITEM_KEY = "SELECTED_ITEM_KEY";
    private FragmentDetailsBinding binding;

    public static DetailsFragment newInstance(ArticlesItem item) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(SELECTED_ITEM_KEY, item);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_details, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ArticlesItem item = (ArticlesItem) getArguments().getSerializable(SELECTED_ITEM_KEY);
        if (item != null) {
            Picasso.get().load(item.getUrlToImage()).into(binding.galleryImage);
            binding.setNews(item);
        }
    }
}