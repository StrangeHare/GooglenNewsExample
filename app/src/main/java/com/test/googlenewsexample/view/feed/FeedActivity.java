package com.test.googlenewsexample.view.feed;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.test.googlenewsexample.R;
import com.test.googlenewsexample.view.BaseActivity;
import com.test.googlenewsexample.viewmodel.FeedViewModel;

import javax.inject.Inject;

import static com.test.googlenewsexample.view.feed.FeedFragment.FEED_FRAGMENT_TAG;

public class FeedActivity extends BaseActivity {

    @Inject
    FeedViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLifecycle().addObserver(viewModel);

        DataBindingUtil.setContentView(this, R.layout.activity_feed);
        FeedFragment fragment = (FeedFragment) getFragmentManager().findFragmentByTag(FEED_FRAGMENT_TAG);
        if (fragment == null) {
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.container , FeedFragment.newInstance(), FEED_FRAGMENT_TAG)
                    .commit();
        }
    }
}
