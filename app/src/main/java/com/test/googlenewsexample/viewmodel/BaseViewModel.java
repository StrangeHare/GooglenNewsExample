package com.test.googlenewsexample.viewmodel;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ViewModel;

import io.reactivex.disposables.CompositeDisposable;

/**
 * @author StrangeHare
 * Date: 13.05.2018
 */
public class BaseViewModel extends ViewModel implements LifecycleObserver {

    CompositeDisposable disposables = new CompositeDisposable();

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onDestroy() {
        disposables.dispose();
    }
}
