package com.test.googlenewsexample.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.test.googlenewsexample.model.ArticlesItem;
import com.test.googlenewsexample.model.News;
import com.test.googlenewsexample.network.repository.NewsRepository;
import com.test.googlenewsexample.view.feed.FeedAdapter;
import com.test.googlenewsexample.view.feed.IFeedView;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * @author StrangeHare
 * Date: 13.05.2018
 */
public class FeedViewModel extends BaseViewModel {

    private NewsRepository repository = new NewsRepository();
    private FeedAdapter adapter = new FeedAdapter();
    private IFeedView view;
    private MutableLiveData<News> data;

    public FeedAdapter getAdapter() {
        return adapter;
    }

    public LiveData<News> getLiveData() {
        if (data == null) {
            data = new MutableLiveData<>();
            loadData();
        }
        return data;
    }

    public void addData(List<ArticlesItem> data) {
        adapter.addData(data);
    }

    public void setView(IFeedView view) {
        this.view = view;
    }

    private void loadData() {
        disposables.add(repository.getAllNews()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(news -> data.postValue(news), error -> view.requestWithError()));
    }
}
