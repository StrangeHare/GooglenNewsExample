package com.test.googlenewsexample.utils;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.test.googlenewsexample.R;

/**
 * @author StrangeHare
 * Date: 13.05.2018
 */
public class SnackBarUtils {

    public static void showSnackbar(Context context, View view, String s) {
        Snackbar snack = Snackbar.make(view, s, Snackbar.LENGTH_LONG);
        View sbView = snack.getView();
        sbView.setBackgroundColor(ContextCompat.getColor(context, R.color.gray_dark));
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(context, R.color.white));
        snack.show();
    }
}
