package com.test.googlenewsexample.network.repository;

import com.test.googlenewsexample.model.News;

import io.reactivex.Observable;

/**
 * @author StrangeHare
 * Date: 13.05.2018
 */
interface INewsRepository {

    Observable<News> getAllNews();
}
