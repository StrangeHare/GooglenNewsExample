package com.test.googlenewsexample.network;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author StrangeHare
 * Date: 13.05.2018
 */
public class ApiCreator {

    public static final String API_KEY = "d0e5183f923c47ceaaed317a274ee39c";
    private static final String BASE_URL = "https://newsapi.org/v2/";
    private OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
    private Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create());

    public NewsApi createNewsApi() {
        return initRetrofit().create(NewsApi.class);
    }

    private Retrofit initRetrofit() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return retrofitBuilder
                .baseUrl(ApiCreator.BASE_URL)
                .client(clientBuilder.addInterceptor(interceptor).build())
                .build();
    }
}
