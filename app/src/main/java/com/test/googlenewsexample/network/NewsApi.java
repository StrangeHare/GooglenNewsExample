package com.test.googlenewsexample.network;

import com.test.googlenewsexample.model.News;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * @author StrangeHare
 * Date: 13.05.2018
 */
public interface NewsApi {

    @GET("top-headlines")
    Observable<News> getAllNews(
            @Query("country") String country,
            @Query("apiKey") String apiKey);
}
