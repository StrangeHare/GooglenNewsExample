package com.test.googlenewsexample.network.repository;

import com.test.googlenewsexample.model.News;
import com.test.googlenewsexample.network.ApiCreator;
import com.test.googlenewsexample.network.NewsApi;

import io.reactivex.Observable;

/**
 * @author StrangeHare
 * Date: 13.05.2018
 */
public class NewsRepository implements INewsRepository {

    private NewsApi api = new ApiCreator().createNewsApi();
    private News news;

    @Override
    public Observable<News> getAllNews() {
        if (news == null) {
            return api.getAllNews("ru", ApiCreator.API_KEY).doOnNext(it -> news = it);
        } else {
            return Observable.just(news)
                    .mergeWith(api.getAllNews("ru", ApiCreator.API_KEY))
                    .doOnNext(it -> news = it);
        }
    }
}
